import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Route, Switch, Redirect } from 'react-router-native'

import Home from './scenes/home';
import Login from './scenes/login';
import Signup from './scenes/signup';
import { isWeb } from './helpers';

let Router;
if (isWeb) {
  Router = require('react-router-dom').BrowserRouter;
} else {
  Router = require('react-router-native').NativeRouter;
}

class App extends Component {
  render() {
    return (
      <Router>
        <View style={styles.container}>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/Signup" component={Signup} />
            <Route path="/Home" component={Home} />
            <Redirect to="/" />
          </Switch>
        </View>
      </Router>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    position: 'relative',
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    width: '100%',
    overflow: 'hidden',
    display: 'flex',
  },
  logo: {
    width: 300,
    height: 300,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default App;

