import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import firebaseService from '../utility/firebase';
import { Route } from 'react-router-dom';

export default class Home extends Component {

    constructor(props) {
        super(props)

        this.state = {
            user: null
        }
    }

    componentDidMount() {
        let user = firebaseService.auth().currentUser
        console.log("user details in home page", user)
        if (user) {
            this.setState({
                user: {
                    email: user.email,
                    uid: user.uid,
                    tokn: user.refreshToken
                }
            })
        } else {

        }
    }

    render() {

        const { user } = this.state;
        return (
            <View style={styles.container}>
                <Text>Welcome {user ? user.email : 'Guest'} </Text>
                <Route render={({ history }) => (
                    <TouchableOpacity style={{backgroundColor:'black', height:40, width:100,marginTop:25, alignItems:'center', justifyContent:'center'}} onPress={()=> history.replace('Login')}>
                        <Text style={{color:'white', fontSize:16, textAlign:'center' }}>Logout</Text>
                    </TouchableOpacity>
                )} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        width: '100%',
    },
    logo: {
        width: 300,
        height: 300,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16,
    },
});