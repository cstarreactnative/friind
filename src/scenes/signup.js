import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
import { Route } from 'react-router-dom';
import { isWeb } from '../helpers';
import firebaseService from '../utility/firebase';

export default class Signup extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: ''
        }
    }

    async signup(history) {
        const { email, password } = this.state;
        if (email !== '' && password !== '') {
            try {
                let user = await firebaseService.auth().createUserWithEmailAndPassword(email, password)
                console.log('user', user)
                if (user) {
                    history.push('Home')
                }
            } catch (error) {
                console.log(error.toString())
                alert(error.toString())
            }
        } else {
            alert('All fields are required.')
        }
    }

    render() {
        const { email, password } = this.state;
        return (
            <View style={styles.container}>
                <Text style={styles.logo}>Friind</Text>
                <TextInput
                    value={email}
                    placeholder={'Email'}
                    onChangeText={(email) => this.setState({ email })}
                    underlineColorAndroid={'transparent'}
                    style={[styles.input, styles.font]}
                />
                <TextInput
                    value={password}
                    placeholder={'Password'}
                    onChangeText={(password) => this.setState({ password })}
                    secureTextEntry={true}
                    style={[styles.input, styles.font]}
                    underlineColorAndroid={'transparent'}
                />
                <Route render={({ history }) => (
                    <TouchableOpacity style={[styles.input, styles.button]} onPress={() => this.signup(history)}>
                        <Text style={{ color: 'white', fontSize: 20 }}>Signup</Text>
                    </TouchableOpacity>
                )} />
                <Route render={({ history }) => (
                    <Text>Already have an account <Text style={{ color: 'blue' }} onPress={() => history.push('Login')}>Login</Text></Text>
                )} />
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        width: '100%',
    },
    logo: {
        fontSize: isWeb ? 35 : 35,
        color: 'black',
        textAlign: 'center',
        marginTop: 50,
        marginBottom: 20
    },
    title: {
        fontWeight: 'bold',
        fontSize: 16,
    },
    input: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 3,
        height: 40,
        width: isWeb ? '25%' : '80%',
        minWidth: isWeb ? 250 : '80%',
        marginBottom: 10,
        paddingLeft: 10
    },
    font: { fontSize: 18 },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black',
    }
});